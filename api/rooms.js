    
var express = require('express');
var api = express.Router();

api.get('/', function(req, res, next) {

    var sql = "SELECT * from api.room ";
    var values = [];
    if (req.query)
    {
        sql += " WHERE 1=1 ";
    }

    if(req.query.roomName)
    {
        var roomSearch = "%" + req.query.roomName + "%";
        values.push(roomSearch)
        sql += " AND name LIKE ?";
    }

    if (req.query.publicFlag)
    {
        sql += " AND public_flag = ?";
        values.push(req.query.publicFlag);
    }

    if (req.query.limit) 
    {
        sql += " LIMIT " + req.query.limit;
    }

    if (req.query.offset)
    {
        sql += " OFFSET " + req.query.offset;
    }

	connection.query(sql, values, function (error, results, fields) {
        if(error){
            res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
            //If there is error, we send the error in the error section with 500 status
        } else if (results == null) {
            res.send(JSON.stringify({"status": 404, "error": null, "response": null}));
        } else {
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        //If there is no error, all is good and response is 200OK.
        }
  	});
});

api.post('/', function(req, res, next) {

    try {
        var values = [Object.values(req.body)];

        connection.query(' INSERT INTO api.room (name, public_flag) VALUES ? ', [values], function (error, results, fields) {
            if(error){
                res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
                //If there is error, we send the error in the error section with 500 status
            } else if (results == null) {
                res.send(JSON.stringify({"status": 404, "error": null, "response": results}));
            } else {

            // TODO: add create conversation settings here
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        //If there is no error, all is good and response is 200OK.
            }
        });
        res.send(JSON.stringify({"status": 400, "error": null, "response": null}));
        
    } catch (e)
    {
        console.log(e);
    }
});

api.get('/:id/messages', function(req, res, next) {

    var sql = " SELECT * FROM api.room r JOIN api.rom_message rm ";
    sql += " ON r.id = rm.room_id"
    var values = [];
    if (req.query)
    {
        sql += " WHERE 1=1 ";
    }

    if(req.query.roomName)
    {
        var roomSearch = "%" + req.query.roomName + "%";
        values.push(roomSearch)
        sql += " AND name LIKE ?";
    }

    if (req.query.publicFlag)
    {
        sql += " AND public_flag = ?";
        values.push(req.query.publicFlag);
    }

    if (req.query.limit) 
    {
        sql += " LIMIT " + req.query.limit;
    }

    if (req.query.offset)
    {
        sql += " OFFSET " + req.query.offset;
    }

	connection.query(sql, values, function (error, results, fields) {
        if(error){
            res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
            //If there is error, we send the error in the error section with 500 status
        } else if (results == null) {
            res.send(JSON.stringify({"status": 404, "error": null, "response": null}));
        } else {
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        //If there is no error, all is good and response is 200OK.
        }
  	});
});

api.post('/:id/messages', function(req, res, next) {

    try {
        var values = [Object.values(req.body)];

        connection.query(' INSERT INTO api.room_message (room_id, conversation_id, text) VALUES ? ', [values], function (error, results, fields) {
            if(error){
                res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
                //If there is error, we send the error in the error section with 500 status
            } else if (results == null) {
                res.send(JSON.stringify({"status": 404, "error": null, "response": results}));
            } else {

            // TODO: add create conversation settings here
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        //If there is no error, all is good and response is 200OK.
            }
        });
        res.send(JSON.stringify({"status": 400, "error": null, "response": null}));
        
    } catch (e)
    {
        console.log(e);
    }
});


module.exports = api;