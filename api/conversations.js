var express = require('express');
var api = express.Router();

api.get('/', function(req, res, next) {

    var sql = "SELECT * from api.conversation ";
    var values = [];
    if (req.query)
    {
        sql += " WHERE 1=1 "
    }

    if(req.query.userID)
    {
        sql += " AND (user_one_id = ? OR user_two_id = ?) ";
        values.push(req.query.userID);
    }

    if (req.query.conversationType)
    {
        sql += " AND conversation_type = ? "
    }

    if (req.query.limit) 
    {
        sql += " LIMIT " + req.query.limit;
    }

    if (req.query.offset)
    {
        sql += " OFFSET " + req.query.offset;
    }

	connection.query(sql, values, function (error, results, fields) {
        if(error){
            res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
            //If there is error, we send the error in the error section with 500 status
        } else if (results == null) {
            res.send(JSON.stringify({"status": 404, "error": null, "response": null}));
        } else {
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        //If there is no error, all is good and response is 200OK.
        }
  	});
});

api.get('/:id/settings', function(req, res, next) {

    var sql = "SELECT * FROM api.conversation_settings WHERE user_id = ?  ";
    var values = [req.params.id];
    
    if(req.query.enabledFlag)
    {
        sql += " AND enabled_flag = ? ";
        values.push(req.query.enabledFlag);
    }

    if (req.query.hiddenFlag)
    {
        sql += " AND hidden_flag = ? ";
        values.push(req.query.enabledFlag);
    }


    if (req.query.limit) 
    {
        sql += " LIMIT " + req.query.limit;
    }

    if (req.query.offset)
    {
        sql += " OFFSET " + req.query.offset;
    }

    console.log(values);

	connection.query(sql, values, function (error, results, fields) {
        if(error){
            res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
            //If there is error, we send the error in the error section with 500 status
        } else if (results == null) {
            res.send(JSON.stringify({"status": 404, "error": null, "response": null}));
        } else {
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        //If there is no error, all is good and response is 200OK.
        }
  	});
});



api.post('/', function(req, res, next) {

    try {
        var values = [Object.values(req.body)];

        connection.query(' INSERT INTO api.conversation (user_one_id, user_two_id, room_id, conversation_type) VALUES ? ', [values], function (error, results, fields) {
            if(error){
                res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
                //If there is error, we send the error in the error section with 500 status
            } else if (results == null) {
                res.send(JSON.stringify({"status": 404, "error": null, "response": results}));
            } else {

            // TODO: add create conversation settings here
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        //If there is no error, all is good and response is 200OK.
            }
        });
        res.send(JSON.stringify({"status": 400, "error": null, "response": null}));
        
    } catch (e)
    {
        console.log(e);
    }

	
});

function createConversationSettings(userID, conversationID)
{
    try {
        var sql = " INSERT INTO api.conversation_setting (user_id, conversation_id) VALUES (?,?)";
        var values = [userID, conversationID];
        connection.query(sql, values), function (error, results, fields) {
            if (error) {
                res.send(JSON.stringify({ "status": 500, "error": error, "response": null }));
            }
        }
    } catch (e) {
        console.log(e);
    }
}

api.get('/:id/messages', function(req, res, next) {

    var sql = "SELECT cm.* FROM api.conversation c JOIN api.conversation_message cm ";
    sql +=  " ON c.id = cm.conversation_id WHERE cm.conversation_id = ? ";
    var values = [req.path.id];

    if (req.query.limit) 
    {
        sql += " LIMIT " + req.query.limit;
    }

    if (req.query.offset)
    {
        sql += " OFFSET " + req.query.offset;
    }

	connection.query(sql, values, function (error, results, fields) {
        if(error){
            res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
            //If there is error, we send the error in the error section with 500 status
        } else if (results == null) {
            res.send(JSON.stringify({"status": 404, "error": null, "response": null}));
        } else {
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        //If there is no error, all is good and response is 200OK.
        }
  	});
});

api.post('/:id/messages', function(req, res, next) {

    try {
        var values = [Object.values(req.body)];

        connection.query(' INSERT INTO api.conversation_message (conversation_id, user_id, text) VALUES ? ', [values], function (error, results, fields) {
            if(error){
                res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
                //If there is error, we send the error in the error section with 500 status
            } else if (results == null) {
                res.send(JSON.stringify({"status": 404, "error": null, "response": results}));
            } else {

            // TODO: add create conversation settings here
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        //If there is no error, all is good and response is 200OK.
            }
        });
        res.send(JSON.stringify({"status": 400, "error": null, "response": null}));
        
    } catch (e)
    {
        console.log(e);
    }
});

module.exports = api;