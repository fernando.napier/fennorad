    
var express = require('express');
var api = express.Router();

api.get('/', function(req, res, next) {

    var sql = "SELECT * from user ";
    var values = [];
    if (req.query)
    {
        sql += " WHERE 1=1 "
    }

    if(req.query.userName)
    {
        var userSearch = "%" + req.query.userName + "%";
        values.push(userSearch)
        sql += " AND name LIKE ?"
    }

    if (req.query.limit) 
    {
        sql += " LIMIT " + req.query.limit;
    }

    if (req.query.offset)
    {
        sql += " OFFSET " + req.query.offset;
    }

	connection.query(sql, values, function (error, results, fields) {
        if(error){
            res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
            //If there is error, we send the error in the error section with 500 status
        } else if (results == null) {
            res.send(JSON.stringify({"status": 404, "error": null, "response": null}));
        } else {
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        //If there is no error, all is good and response is 200OK.
        }
  	});
});

api.get('/:id', function(req, res, next) {

    var sql = "SELECT * FROM user WHERE user_id = ?  ";
    var values = [req.params.id];      

    console.log(values);

	connection.query(sql, values, function (error, results, fields) {
        if(error){
            res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
            //If there is error, we send the error in the error section with 500 status
        } else if (results == null) {
            res.send(JSON.stringify({"status": 404, "error": null, "response": null}));
        } else {
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        //If there is no error, all is good and response is 200OK.
        }
  	});
});



api.post('/', function(req, res, next) {

    try {
        var values = [Object.values(req.body)];
        const username = req.body.username;
        const password = req.body.password;

        if (username && password) 
        {
            
            
            

            connection.query(' INSERT INTO api.user (user_name, password, email) VALUES ? ', [values], function (error, results, fields) {
                if(error){
                    res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
                    //If there is error, we send the error in the error section with 500 status
                } else if (results == null) {
                    res.send(JSON.stringify({"status": 404, "error": null, "response": results}));
                } else {
                res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
            //If there is no error, all is good and response is 200OK.
                }
            });
        } else {
            res.send(JSON.stringify({"status": 400, "error": null, "response": null}));
        }
    } catch (e)
    {
        console.log(e);
    }

	
});

api.get('/:id/messages', function(req, res, next) {

    var sql = "SELECT cm.* FROM api.conversation c JOIN api.conversation_message cm ";
    sql +=  " ON c.id = cm.conversation_id WHERE cm.conversation_id = ? ";
    var values = [req.path.id];

    if (req.query.limit) 
    {
        sql += " LIMIT " + req.query.limit;
    }

    if (req.query.offset)
    {
        sql += " OFFSET " + req.query.offset;
    }

	connection.query(sql, values, function (error, results, fields) {
        if(error){
            res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
            //If there is error, we send the error in the error section with 500 status
        } else if (results == null) {
            res.send(JSON.stringify({"status": 404, "error": null, "response": null}));
        } else {
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        //If there is no error, all is good and response is 200OK.
        }
  	});
});

api.post('/:id/messages', function(req, res, next) {

    try {
        var values = [Object.values(req.body)];

        connection.query(' INSERT INTO api.conversation_message (conversation_id, user_id, text) VALUES ? ', [values], function (error, results, fields) {
            if(error){
                res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
                //If there is error, we send the error in the error section with 500 status
            } else if (results == null) {
                res.send(JSON.stringify({"status": 404, "error": null, "response": results}));
            } else {

            // TODO: add create conversation settings here
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        //If there is no error, all is good and response is 200OK.
            }
        });
        res.send(JSON.stringify({"status": 400, "error": null, "response": null}));
        
    } catch (e)
    {
        console.log(e);
    }
});

module.exports = api;