const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const Chatkit = require('pusher-chatkit-server')
const uuidV4 = require('uuid/v4')
const mysql = require('mysql');

const app = express()
const api = express.Router();
const health = require('./api/health')
const users = require('./api/users')
const rooms = require('./api/rooms')
const conversations = require('./api/conversations')




app.use(function(req, res, next){
  global.connection = mysql.createConnection({
    host: 'localhost',
    user: 'devuser',
    password: '#NorthwestPassage',
    database: 'api'
  });
  connection.connect();
  next();
})




/* const chatkit = new Chatkit.default({
  instanceLocator: 'v1:us1:431c22b9-6359-4044-a58f-cdda5e9f0411',
  key: '6d588685-e0c1-4bef-b181-eea85a4363e4:cmQ2Xgp76oCQ95+BsrR9g8W/LGniZJ/vhXQnLJDsBHQ=',
}) */

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())

// set route names
app.use('/v1/users', users)
app.use('/v1/rooms', rooms)
app.use('/v1/health', health)
app.use('/v1/conversations', conversations)


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json(err);
});

const PORT = 3001
app.listen(PORT, err => {
  if (err) {
    console.error(err)
  } else {
    console.log(`Running on port ${PORT}`)
  }
})

module.exports = api;