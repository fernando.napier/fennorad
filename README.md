# fennorad

This is meant to be a simple exercise into creating a project that encompasses full iterative application development. This includes writing a front-end (React), a back-end API (Express), and db (MySQL). It includes CI/CD and deployment via docker containers and ultimately deployed to 'a cloud'

This app is a messaging app which allows users to communicate directly with one another as well as publicly through rooms.
