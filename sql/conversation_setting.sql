CREATE TABLE `api`.`conversation_setting` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `conversation_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  `enable_flag` TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'this is a flag that means the user either initiated the conversation or accepted the message received' ,
  `hidden_flag` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'this flag represents whether the user wants the conversation to show on their recent conversations or not' ,
  `notification_flag` TINYINT(1) NOT NULL DEFAULT 1
  PRIMARY KEY (`id`));