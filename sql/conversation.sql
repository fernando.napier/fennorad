CREATE TABLE `api`.`conversation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_one_id` INT NOT NULL,
  `user_two_id` VARCHAR(45) NULL,
  `room_id` INT NULL,
  `conversation_type` ENUM("USER_TO_USER", "ROOM") NOT NULL,
  PRIMARY KEY (`id`));
