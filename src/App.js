import React from 'react'
import UsernameForm from './components/UsernameForm'

class App extends React.Component {
  constructor () {
    super()
    this.onUsernameSubmitted = this.onUsernameSubmitted.bind(this)
  }
  
  onUsernameSubmitted(username, password) {
    var email = "";
    fetch('http://localhost:3001/v1/users', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({username, password, email})
    }).then(response => {
      console.log('success')
    })
    .catch(error => {
      console.error(error)
    })
  }
  render() {
    return (
      <div className="app">
          <UsernameForm onSubmit={this.onUsernameSubmitted}/>
      </div>
    );
  }
}

export default App
