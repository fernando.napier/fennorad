import React from 'react'

class UsernameForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
    }

    this.onUsernameChange = this.onUsernameChange.bind(this)
    this.onPasswordChange = this.onPasswordChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onUsernameChange(e) {
    this.setState({
      username: e.target.value,
    })
  }

  onPasswordChange(e) {
    this.setState({
      password: e.target.value,
    })
  }

  onSubmit(e) {
    e.preventDefault()
    console.log(this.state)
    this.props.onSubmit(this.state.username, this.state.password)
  }

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <input type="text" placeholder="What is your username?" onChange={this.onUsernameChange}/>
          <input type="text" placeholder="What is your password?" onChange={this.onPasswordChange}/>
          <input type="submit"/>
        </form>
      </div>
    )
  }
}

export default UsernameForm
