#!/bin/bash

# any future command that fails will exit the script
set -e

# set docker fix for cert directory
# DOCKER_TLS_CERTDIR: ""

# this is a fix for post-install issues
git config --global core.ignorecase false

# Delete the old repo
rm -rf /home/ubuntu/fennorad/

# clone the repo again
git clone https://gitlab.com/fernando.napier/fennorad.git

#source the nvm file. In an non
#If you are not using nvm, add the actual path like
# PATH=/home/ubuntu/node/bin:$PATH
#source /home/ubuntu/.nvm/nvm.sh

#sudo npm i -g pm2
# stop the previous pm2

#sudo pm2 kill
#sudo npm remove pm2 -g

# allow npm install to write to /node_modules
sudo chown -R $USER /usr/local

#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
npm install pm2 -g
# starting pm2 daemon
pm2 status

cd /home/ubuntu/fennorad

#install npm packages
echo "Running npm install"
npm install

#Restart the node server
echo "Running npm start"
npm run build
npm start